package com.me.biggerpantest.presentation.news;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.me.biggerpantest.data.Article;
import com.me.biggerpantest.databinding.FragmentNewsBinding;

import java.util.List;


public class NewsFragment extends Fragment implements NewsFragmentContract.View {

    private FragmentNewsBinding binding;
    private LinearLayoutManager layoutManager;
    private NewsAdapter newsAdapter;
    private NewsFragmentContract.Presenter presenter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentNewsBinding.inflate(inflater, container, false);

        layoutManager = new LinearLayoutManager(getActivity());
        binding.recycler.setLayoutManager(layoutManager);
        newsAdapter = new NewsAdapter();
        binding.recycler.setAdapter(newsAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        binding.recycler.addItemDecoration(dividerItemDecoration);

        return binding.getRoot();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @Override
    public void showNewPageAvailable(List<Article> url) {
        newsAdapter.setNewsData(url);
    }

    @Override
    public void showMessage(int messageResId) {
        Toast.makeText(getContext(), messageResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(NewsFragmentContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detach();
    }

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }
}
