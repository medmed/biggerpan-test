package com.me.biggerpantest.presentation.brower;

import androidx.annotation.NonNull;

import com.me.biggerpantest.R;
import com.me.biggerpantest.data.TagRepository;

import org.jsoup.HttpStatusException;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BrowserFragmentPresenter implements BrowserFragmentContract.Presenter {

    private final CompositeDisposable compositeDisposable;

    @NonNull
    public final BrowserFragmentContract.View view;
    public final TagRepository tagRepository;

    public BrowserFragmentPresenter(
            @NonNull BrowserFragmentContract.View view,
            @NonNull TagRepository tagRepository
    ) {
        this.view = view;
        this.tagRepository = tagRepository;
        this.view.setPresenter(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void fetchTagsForUrl(String url) {
        compositeDisposable.add(tagRepository.fetchTagListByUrl(url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleTagResult, this::handleError));
    }

    private void handleError(Throwable throwable) {
        if (throwable instanceof HttpStatusException) {
            view.showMessage(R.string.error_loading_tags);
        } else {
            view.showMessage(R.string.navigation_error);
        }
    }

    private void handleTagResult(List<String> tags) {
        if (tags.isEmpty()) {
            view.showNoTagFound();
            view.showMessage(R.string.tag_not_found);
        } else {
            view.showNewTagFound(tags);
        }
    }

    @Override
    public void newUrlTyped(String url) {
        if (url.matches("([a-z]+:\\/\\/)?[a-z 0-9]+\\.[a-z]{2,}.*")) {
            boolean hasHttpSuffix = url.contains("http://");
            boolean hasHttpsSuffix = url.contains("https://");

            if (hasHttpSuffix) {
                url = url.replace("http://", "https://");
            } else if (!hasHttpsSuffix) {
                url = "https://".concat(url);
            }

            view.loadUrl(url);
        } else {
            view.showToastMessage(R.string.url_not_valid);
        }
    }

    @Override
    public void init() {

    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
    }
}
