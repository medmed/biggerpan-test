package com.me.biggerpantest.presentation;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.me.biggerpantest.R;
import com.me.biggerpantest.data.TagRepositoryImpl;
import com.me.biggerpantest.databinding.ActivityMainBinding;
import com.me.biggerpantest.presentation.brower.BrowserFragment;
import com.me.biggerpantest.presentation.brower.BrowserFragmentPresenter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BrowserFragment browserFragment = (BrowserFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (browserFragment == null) {
            browserFragment = BrowserFragment.newInstance();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.contentFrame, browserFragment);
            transaction.commit();
        }

        new BrowserFragmentPresenter(
                browserFragment,
                new TagRepositoryImpl()
        );
    }
}