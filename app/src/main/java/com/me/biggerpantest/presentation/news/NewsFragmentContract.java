package com.me.biggerpantest.presentation.news;

import androidx.annotation.StringRes;

import com.me.biggerpantest.BasePresenter;
import com.me.biggerpantest.BaseView;
import com.me.biggerpantest.data.Article;

import java.util.List;

public interface NewsFragmentContract {

    interface View extends BaseView<Presenter> {

        void showNewPageAvailable(List<Article> url);

        void showMessage(@StringRes int messageResId);

        void showLoading();

        void hideLoading();

    }

    interface Presenter extends BasePresenter {


    }
}
