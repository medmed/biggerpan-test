package com.me.biggerpantest.presentation.brower;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.me.biggerpantest.BasePresenter;
import com.me.biggerpantest.BaseView;
import com.me.biggerpantest.data.Article;

import java.util.List;

public interface BrowserFragmentContract {

    interface View extends BaseView<Presenter> {

        void showNewTagFound(List<String> tags);

        void showMessage(@StringRes int messageResId);

        void showToastMessage(@StringRes int messageResId);

        void loadUrl(String url);

        void showNoTagFound();
    }

    interface Presenter extends BasePresenter {

        void fetchTagsForUrl(String url);

        void newUrlTyped(String url);
    }
}
