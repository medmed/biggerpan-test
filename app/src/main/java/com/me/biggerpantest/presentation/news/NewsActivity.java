package com.me.biggerpantest.presentation.news;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.me.biggerpantest.R;
import com.me.biggerpantest.data.NewsApi;
import com.me.biggerpantest.data.NewsRepositoryImpl;
import com.me.biggerpantest.databinding.ActivityMainBinding;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsActivity extends AppCompatActivity {

    public static final String TAGS_KEY = "tags";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        NewsFragment newsFragment = (NewsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (newsFragment == null) {
            newsFragment = NewsFragment.newInstance();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.contentFrame, newsFragment);
            transaction.commit();
        }

        String[] tags = getIntent().getStringArrayExtra(TAGS_KEY);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        NewsApi newsApi = retrofit.create(NewsApi.class);

        new NewsFragmentPresenter(
                newsFragment,
                new NewsRepositoryImpl(newsApi),
                tags
        );
    }
}
