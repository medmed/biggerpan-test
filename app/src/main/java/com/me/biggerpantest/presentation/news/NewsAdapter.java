package com.me.biggerpantest.presentation.news;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.me.biggerpantest.R;
import com.me.biggerpantest.data.Article;
import com.me.biggerpantest.databinding.RecyclerNewsItemBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsItem> {

    private final ArrayList<Article> data = new ArrayList();

    @NonNull
    @Override
    public NewsItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerNewsItemBinding inflate = RecyclerNewsItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new NewsItem(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsItem holder, int position) {
        holder.setTitle(data.get(position).title);
        holder.setDescription(data.get(position).description);
        holder.setImage(data.get(position).urlToImage);
        holder.setLink(data.get(position).url);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setNewsData(List<Article> url) {
        data.clear();
        data.addAll(url);
        notifyDataSetChanged();
    }

    static class NewsItem extends RecyclerView.ViewHolder {

        private RecyclerNewsItemBinding itemView;

        public NewsItem(@NonNull RecyclerNewsItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }

        public void setTitle(String title) {
            itemView.title.setText(TextUtils.concat(title.substring(0, Math.min(title.length(), 40))));
        }

        public void setDescription(String description) {
            itemView.description.setText(TextUtils.concat(description.substring(0, Math.min(description.length(), 40)).replace("\n", ""), "..."));
        }

        public void setImage(String urlToImage) {
            if (urlToImage == null) {
                itemView.imageView.setImageResource(R.drawable.ic_baseline_block_24);
                return;
            }
            Picasso.get()
                    .load(urlToImage)
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_baseline_block_24)
                    .into(itemView.imageView);
        }

        public void setLink(String url) {
            if (url == null) {
                return;
            }
            itemView.getRoot().setOnClickListener((v) -> {
                v.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url)));
            });
        }
    }
}

