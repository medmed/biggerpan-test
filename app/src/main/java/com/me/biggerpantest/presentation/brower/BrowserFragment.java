package com.me.biggerpantest.presentation.brower;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebViewClientCompat;

import com.google.android.material.snackbar.Snackbar;
import com.me.biggerpantest.R;
import com.me.biggerpantest.databinding.FragmentBrowserBinding;
import com.me.biggerpantest.presentation.news.NewsActivity;

import java.util.List;

public class BrowserFragment extends Fragment implements BrowserFragmentContract.View {

    private BrowserFragmentContract.Presenter presenter;
    private FragmentBrowserBinding binding;
    private OnBackPressedCallback onBackPressedCallback;

    @Override
    public android.view.View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentBrowserBinding.inflate(inflater, container, false);

        binding.backBtn.setOnClickListener((view) -> binding.webview.goBack());
        binding.nextBtn.setOnClickListener((view) -> binding.webview.goForward());
        binding.refreshBtn.setOnClickListener((view) -> binding.webview.reload());

        binding.webview.loadUrl("https://www.google.com");

        onBackPressedCallback = new OnBackPressedCallback(false) {
            @Override
            public void handleOnBackPressed() {
                binding.backBtn.callOnClick();
                setBackPressedListenerActivation();
            }
        };

        binding.webview.setWebViewClient(new WebViewClientCompat() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                binding.urlEditText.getEditText().setText(url.replace("https://",""));
                setBackPressedListenerActivation();
                presenter.fetchTagsForUrl(url);
            }
        });

        binding.floatingActionButton.hide();

        binding.urlEditText.getEditText().setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                String url = v.getText().toString().toLowerCase();
                presenter.newUrlTyped(url);
                v.clearFocus();
                return false;
            }
            return false;
        });


        requireActivity().getOnBackPressedDispatcher().addCallback(onBackPressedCallback);

        return binding.getRoot();
    }

    private void setBackPressedListenerActivation() {
        onBackPressedCallback.setEnabled(binding.webview.canGoBack());
    }

    @Override
    public void showMessage(@StringRes int messageResId) {
        View view = getView();
        if (view == null) {
            return;
        }
        Snackbar.make(view, getString(messageResId), Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void showNewTagFound(List<String> tags) {
        binding.floatingActionButton.show();
        binding.floatingActionButton.setImageDrawable(getContext().getDrawable(R.drawable.ic_baseline_find_in_page_24));
        binding.floatingActionButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), NewsActivity.class);
            intent.putExtra(NewsActivity.TAGS_KEY, tags.toArray(new String[0]));
            startActivity(intent);
        });
    }

    @Override
    public void showToastMessage(@StringRes int messageResId) {
        Toast.makeText(getContext(), messageResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadUrl(String url) {
        binding.webview.loadUrl(url);
    }

    @Override
    public void showNoTagFound() {
        binding.floatingActionButton.hide();
        binding.floatingActionButton.setOnClickListener(null);
    }

    @Override
    public void setPresenter(BrowserFragmentContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detach();
    }

    public static BrowserFragment newInstance() {
        return new BrowserFragment();
    }

}
