package com.me.biggerpantest.presentation.news;


import androidx.annotation.NonNull;

import com.me.biggerpantest.R;
import com.me.biggerpantest.data.NewsRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NewsFragmentPresenter implements NewsFragmentContract.Presenter {

    private final CompositeDisposable compositeDisposable;
    public final NewsRepository newsRepository;
    public final String[] tags;


    public final NewsFragmentContract.View view;

    public NewsFragmentPresenter(
            @NonNull NewsFragmentContract.View view,
            @NonNull NewsRepository newsRepository,
            String[] tags) {
        this.view = view;
        this.newsRepository = newsRepository;
        this.tags = tags;
        compositeDisposable = new CompositeDisposable();
        this.view.setPresenter(this);
        fetchNewsFromTags(this.tags);
    }


    public void fetchNewsFromTags(String[] tags) {
        compositeDisposable.add(newsRepository.fetchNewsFromTags(tags)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(view::showNewPageAvailable, this::handleError)
        );
    }

    private void handleError(Throwable throwable) {
        view.showMessage(R.string.error_loading_news);
    }

    @Override
    public void init() {

    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
    }
}
