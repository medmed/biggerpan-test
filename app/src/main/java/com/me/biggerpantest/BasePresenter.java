package com.me.biggerpantest;

public interface BasePresenter {

    void init();

    void detach();

}