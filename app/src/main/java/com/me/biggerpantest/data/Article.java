package com.me.biggerpantest.data;

public class Article {
    public String title;
    public String description;
    public String urlToImage;
    public String url;
}