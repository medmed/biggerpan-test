package com.me.biggerpantest.data;

import java.util.List;

import io.reactivex.Observable;

public interface NewsRepository {
    Observable<List<Article>> fetchNewsFromTags(String[] tags);
}
