package com.me.biggerpantest.data;

import androidx.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;

public interface TagRepository {

    Observable<List<String>> fetchTagListByUrl(@NonNull String url);
}
