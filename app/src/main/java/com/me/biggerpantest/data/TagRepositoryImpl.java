package com.me.biggerpantest.data;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class TagRepositoryImpl implements TagRepository {

    @Nullable
    @Override
    public Observable<List<String>> fetchTagListByUrl(@NonNull String url) {
        return Observable.just(Jsoup.connect(url))
                .map(Connection::get)
                .map(doc -> {
                    Elements elemts = doc.select("meta");
                    ArrayList<String> tags = new ArrayList<>();
                    for (Element e : elemts) {
                        if (TextUtils.equals(e.attributes().get("property"), "article:tag")) {
                            tags.add(e.attributes().get("content"));
                        }
                    }

                    return tags;
                });
    }
}
