package com.me.biggerpantest.data;

import android.text.TextUtils;

import java.util.List;

import io.reactivex.Observable;

public class NewsRepositoryImpl implements NewsRepository {

    private final NewsApi newsApi;

    public NewsRepositoryImpl(NewsApi newsApi) {
        this.newsApi = newsApi;
    }

    @Override
    public Observable<List<Article>> fetchNewsFromTags(String[] tags) {
        String query = TextUtils.join(" OR ", tags);
        return newsApi
                .listRepos(query)
                .map(data -> data.articles);
    }
}


