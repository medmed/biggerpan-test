package com.me.biggerpantest.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {
    @GET("v2/everything?sortBy=publishedAt&apiKey=650c61428d884770982ca3a9de95f90b")
    Observable<Response> listRepos(@Query("q") String query);

}