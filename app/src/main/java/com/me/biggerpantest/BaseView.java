package com.me.biggerpantest;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
